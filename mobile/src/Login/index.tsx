import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Button, Input, Image } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

class Login extends React.Component<any, any> {
    render() {
        return <View style={styles.wrap}>
            <Image
                source={require('../assets/images/logo.png')}
                style={{width: 1.1137 * 150, height: 150}}
            />

            <Text h3>Login</Text>

            <View style={styles.wrapInputs}>
                <View style={{marginTop: 10}}>
                    <Input placeholder='Email'
                           leftIcon={
                               <Icon
                                   name='user'
                                   size={24}
                                   color='black'
                               />
                           }
                    />
                </View>

                <View style={{marginTop: 10}}>
                    <Input placeholder='Password'
                           leftIcon={
                               <Icon
                                   name='user'
                                   size={24}
                                   color='black'
                               />
                           }
                    />
                </View>
            </View>

            <View style={styles.wrapButtons}>
                <View style={styles.wrapButton}>
                    <Button
                        icon={
                            <Icon
                                name={'home'}
                                size={24}
                                color='black'
                            />
                        }
                        type={"outline"}
                        title="Clear"
                    />
                </View>

                <View style={styles.wrapButton}>
                    <Button
                        icon={
                            <Icon
                                name={'user'}
                                size={24}
                                color='black'
                            />
                        }
                        title="Submit"
                    />
                </View>
            </View>

        </View>;
    }
}

const styles = StyleSheet.create({
    wrap: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column'
    },
    title: {},
    wrapInputs: {
        width: '80%'
    },
    wrapButtons: {
        flexDirection: 'row',
        marginTop: 10
    },
    wrapButton: {
        marginLeft: 10
    }
});

export default Login;