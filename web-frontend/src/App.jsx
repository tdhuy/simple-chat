import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Link, Route } from 'react-router-dom';
import { routes } from "./constants/routes";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Spinner } from "./components/Spinner";
import { SpinnerProvider } from "./components/Spinner/spinner.context";

function App() {
    return (
        <SpinnerProvider>
            <div className="App">
                <Router>
                    <div>
                        <ul style={{position: 'fixed', bottom: 0, left: 0}}>
                            {
                                routes.map((r, index) => {
                                    return <li key={index}
                                               style={{display: 'inline-block', margin: '0 10px'}}>
                                        <Link to={r.path}>{r.name}</Link>
                                    </li>
                                })
                            }
                        </ul>

                        <Switch>
                            {
                                routes.map((r, index) => {
                                    return <Route path={r.path}
                                                  exact={r.path === '/'}
                                                  component={r.component}
                                                  key={index}/>
                                })
                            }
                        </Switch>
                    </div>
                </Router>

                <Spinner/>
            </div>
        </SpinnerProvider>
    );
}

export default App;
