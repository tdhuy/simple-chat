import React from 'react';
import {Link} from 'react-router-dom';
import {Form, Button, Row, Col, Container} from 'react-bootstrap';
import * as API from '../../constants/api';
import axios from 'axios';
import Cookies from 'js-cookie';
import * as CookieNames from '../../constants/cookie';

export default class Login extends React.Component {
    emailInputRef = React.createRef();
    passwordInputRef = React.createRef();

    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onClickedReset = this.onClickedReset.bind(this);
    }

    onClickedReset() {
        this.emailInputRef.current.value = '';
        this.passwordInputRef.current.value = '';
    }

    onSubmit(event) {
        event.preventDefault();
        const formData = {
            email: event.currentTarget.elements.email.value,
            password: event.currentTarget.elements.password.value,
        };

        axios.post(API.Login, formData)
            .then(res => {
                this.saveCookieToken(res.data.token, res.data.user);
                this.props.history.push('/');
            })
            .catch(err => {
                if (err.response.data.message) {
                    alert(err.response.data.message);
                } else {
                    alert(err);
                }
            })
    }

    saveCookieToken(token, userInfo) {
        Cookies.set(CookieNames.Token, token, { expires: 1 });
        Cookies.set(CookieNames.UserInfo, userInfo, { expires: 1 });
    }

    render() {
        return (
            <Container fluid={true}>
                <Row>
                    <Col sm={{offset: 4, span: 4}}>
                        <h3 className="text-center mt-5">
                            Login
                        </h3>

                        <Form onSubmit={this.onSubmit}>
                            <Form.Group controlId="email">
                                <Form.Label>Email</Form.Label>

                                <Form.Control type="text"
                                              ref={this.emailInputRef}
                                              placeholder="Enter email"/>
                            </Form.Group>

                            <Form.Group controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password"
                                              ref={this.passwordInputRef}
                                              placeholder="Password"/>
                            </Form.Group>

                            <div className="text-center">
                                <Button variant="secondary"
                                        onClick={this.onClickedReset}>
                                    Reset
                                </Button>

                                <Button variant="primary"
                                        className="ml-5"
                                        type="submit">
                                    Submit
                                </Button>
                            </div>

                            <div className="text-center">
                                <Link to="/register">
                                    Go to register page.
                                </Link>
                            </div>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}