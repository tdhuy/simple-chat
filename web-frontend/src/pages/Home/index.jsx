import React from 'react';
import style from './style.module.scss';
import { SpinnerAction } from "../../components/Spinner/spinner.context";

export default class Home extends React.Component {

    toggleSpinnerFn = (value) => {}; // init fake show/hide loading function

    componentDidMount() {
        this.toggleSpinnerFn(true); // show loading

        setTimeout(() => {
            this.toggleSpinnerFn(false); // hide loading
        }, 3000)
    }

    render() {
        return (
            <>
                <SpinnerAction.Consumer>
                    {
                        (toggle) => {
                            this.toggleSpinnerFn = toggle; // assign real show/hide function to toggleSpinnerFn
                        }
                    }
                </SpinnerAction.Consumer>

                <div className={style.wrap}>
                    <div className={style.listFriend}>

                    </div>
                    <div className={style.main}>
                        <div className={style.header}>

                        </div>
                        <div className={style.subGroup}>
                            <div className={style.conversation}>
                                <div className={style.conversationMessages}>

                                </div>
                                <div className={style.inputArea}>

                                </div>
                            </div>
                            <div className={style.profile}>

                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}