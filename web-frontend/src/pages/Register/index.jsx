import React from 'react';
import {Link} from 'react-router-dom';
import {Form, Button, Row, Col, Container} from 'react-bootstrap';

export default class Register extends React.Component {
    usernameInputRef = React.createRef();
    passwordInputRef = React.createRef();
    confirmPassInputRef = React.createRef();
    nameInputRef = React.createRef()

    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.onClickedReset = this.onClickedReset.bind(this);
    }

    onClickedReset() {
        this.usernameInputRef.current.value = '';
        this.passwordInputRef.current.value = '';
        this.confirmPassInputRef.current.value = '';
        this.nameInputRef.current.value = '';
    }

    onSubmit(event) {
        event.preventDefault();
        const formData = {
            username: event.currentTarget.elements.username.value,
            password: event.currentTarget.elements.password.value,
            confirmPass: event.currentTarget.elements.confirmPass.value,
            name: event.currentTarget.elements.name.value
        };
        console.log(formData);
    }
    render() {
        return (
            <Container fluid={true}>
                <Row>
                    <Col sm={{offset: 4, span: 4}}>
                        <h3 className="text-center mt-5">
                            Login
                        </h3>

                        <Form onSubmit={this.onSubmit}>
                            <Form.Group controlId="username">
                                <Form.Label>Username</Form.Label>

                                <Form.Control type="text"
                                              ref={this.usernameInputRef}
                                              placeholder="Enter username"/>

                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password"
                                              ref={this.passwordInputRef}
                                              placeholder="Password"/>
                            </Form.Group>

                            <Form.Group controlId="confirmPass">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password"
                                              ref={this.confirmPassInputRef}
                                              placeholder="Confirm Password"/>
                            </Form.Group>

                            <Form.Group controlId="name">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="text"
                                              ref={this.nameInputRef}
                                              placeholder="Name"/>
                            </Form.Group>

                            <div className="text-center">
                                <Button variant="secondary"
                                        onClick={this.onClickedReset}>
                                    Reset
                                </Button>

                                <Button variant="primary"
                                        className="ml-5"
                                        type="submit">
                                    Submit
                                </Button>
                            </div>

                            <div className="text-center">
                                <Link to="/Login">
                                    Go to login page.
                                </Link>
                            </div>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}