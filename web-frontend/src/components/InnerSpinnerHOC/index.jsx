import React from 'react';
import './InnerSpinnerHOC.scss';

/**
 *
 * @param {React.Component} WrappedComponent
 * @param {boolean} showSpinnerLoading
 * @returns {}
 */
const withInnerSpinner = (WrappedComponent, showSpinnerLoading) => {
    return class extends React.Component {
        render() {
            return (
                <div className='inner_spinner'>
                    <WrappedComponent {...this.props} />

                    {
                        showSpinnerLoading && <div className='inner_spinner__loader'>
                            Loading
                        </div>
                    }
                </div>
            )
        }
    }
};