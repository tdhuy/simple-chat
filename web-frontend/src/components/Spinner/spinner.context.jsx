import React, { createContext } from 'react';

export const SpinnerState = createContext(false);
export const SpinnerAction = createContext((value) => {
});

export class SpinnerProvider extends React.Component {

    state = {
        show: false
    };

    toggle(value) {
        this.setState({
            show: value
        })
    }

    render() {
        return (
            <SpinnerState.Provider value={this.state.show}>
                <SpinnerAction.Provider value={(value) => {
                    this.toggle(value);
                }}>
                    {this.props.children}
                </SpinnerAction.Provider>
            </SpinnerState.Provider>
        )
    }
}
