import React from "react";
import { SpinnerState } from './spinner.context';
import './Spinner.scss';

export class Spinner extends React.Component {

    renderSpinnerHTML(isShow) {
        console.log('spinner value', isShow);
        if (isShow) {
            return (
                <div className={'spinner-wrap'}>
                    Loading
                </div>
            )
        }

        return <></>
    }

    render() {
        return (
            <SpinnerState.Consumer>
                {
                    (isShow) => this.renderSpinnerHTML(isShow)
                }
            </SpinnerState.Consumer>
        )
    }
}