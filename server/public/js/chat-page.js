// sample list users
let users = []; // ko thay doi sau khi lay tu api
let filteredUsers = [];
let socket = null;
let currentMessages = [];
let currentGroup = null;

const KEY_CODES = {
  ENTER: 13
};

const elements = {
  listUser: '.sp-left-list-user',
  inputSearchUser: '#input-search-user',
  mainContent: '.sp-main-content',
  conservation: '.sp-conservation-content',
  btnSubmitMsg: '#btn-submit-msg',
  inputMsg: '#input-msg',
  inputFile: '#input-file',
  inputImage: '#input-image'
};

/**
 * Tạo group mới với userId mới và user đang login
 * @param {string} userId
 * @return Promise<any>
 */
function createNewGroup(userId) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: '/api/groups',
      method: 'POST',
      beforeSend: function(request) {
        request.setRequestHeader("token", localStorage.getItem('sp_token'));
      },
      data: {
        userId: userId
      },
      success: (response) => {
        resolve(response);
      },
      error: (error) => {
        reject(error);
      }
    })
  });
}

function sendMessage() {
  toggleLoaderSendingMsg(true);
  const msg = $(elements.inputMsg).val();
  const msgObj = {
    msg,
    isMime: true,
    type: 'TEXT',
    avatar: 'http://placehold.it/50'
  };
  const html = $(htmlBuilder.generageMessage(msgObj));
  socket.emit(SOCKET_NAMES.SEND_MESSAGE, {
    to: currentGroup,
    message: msg,
    type: 'TEXT'
  });
  $(elements.inputMsg).val('');
  $(elements.conservation).append(html);
  toggleLoaderSendingMsg(false);
  // scroll to bottom
  scrollToBottom($(elements.conservation));
}

function filterUsers() {
  const nameText = $(elements.inputSearchUser).val();
  if(!nameText){
    filteredUsers = users;
    renderListUsers();
    return;
  }
  filteredUsers = users.filter(user => {
    if (user.name.toLowerCase().indexOf(nameText) !== -1) {
      return true;
    }

    return false;
  });

  renderListUsers();
}

/**
 * Sự kiện khi click vào user item ở sanh sách user. Có thể xảy ra 2 trường hợp: có groupId hoặc ko có groupId. Không có
 * groupId thì phải userId
 * @param {string} groupId
 * @param {string} userId
 */
function onClickUserItem(groupId, userId) {
  console.log('group id', groupId);
  console.log('user id', userId);
  if (!groupId && !userId) {
    throw new Error('Not enough data');
  }

  if (userId) {
    return createNewGroup(userId)
      .then((response) => {
        currentGroup = response.data.group._id;

        getListUsers();
        loadMessageOfGroup(groupId);
      })
      .catch((err) => {
        console.error(err);
      });
  }

  currentGroup = groupId;
  loadMessageOfGroup(groupId);
  //deleteMessage(groupId);
}

/**
 * Load tin nhắn cữ của group
 * @param {string} groupId
 */
function loadMessageOfGroup(groupId) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: '/api/messages',
      method: 'GET',
      beforeSend: function(request) {
        request.setRequestHeader("token", localStorage.getItem('sp_token'));
      },
      data: {
        groupId: groupId
      },
      success: (res) => {
        $(elements.conservation).empty();
        currentMessages = res.data.map(msg => {

          const msgObj = {
            type: msg.type || 'TEXT',
            avatar: 'http://placehold.it/50',
            isMime: msg.isMime,
            msg: msg.text, // type=TEXT,
            url: msg.image, // type=IMAGE,
            fileLink: msg.file // type=FILE
          };

          if(msg.isMime == true){
            const html = $(htmlBuilder.generageMessage(msgObj));
            $(elements.inputMsg).val('');
            $(elements.conservation).append(html);
            toggleLoaderSendingMsg(false);
            // scroll to bottom
            scrollToBottom($(elements.conservation));
          } else {
            const html = htmlBuilder.generageMessage(msgObj);
            $(elements.conservation).append(html);
          }
        });
      },
      error: (error) => {
        reject(error);
      }
    })
  });
}

function renderListUsers() {
  $(elements.listUser).empty();

  filteredUsers.forEach((user) => {
    const html = $(htmlBuilder.generateUserItem(user));
    html.click(function () {
      const groupId = $(this).data('groupId');  //$(this) chính là biến html mình đang click
      const userId = $(this).data('userId');
      onClickUserItem(groupId, userId);
    });
    //const $html = $(htmlBuilder.generageMessage);

    $(elements.listUser).append(html);
  });
}

function getListUsers() {
  $.ajax({
    url: '/api/users/',
    method: 'GET',
    beforeSend: function(request) {
      request.setRequestHeader("token", localStorage.getItem('sp_token'));
    },
    success: (res) => {
      users = res.data.groupsAndUsers.map(item => {

        const firstCharacter = (item.name || '').charAt(0).toUpperCase();
        item.avatar = item.avatar || `http://placehold.it/50?text=${firstCharacter}` ;

        if (item.type === 'USER') {
          item.userId = item._id;
        }

        if (item.type === 'GROUP') {
          item.groupId = item._id;
        }

        return item;
      });

      console.log(users);

      filteredUsers = users;
      renderListUsers();
    },
    error: (res) => {
      alert(res.responseJSON.message.join('\n'));
    }
  });
}

// upload image and send message by socket
function uploadImageAndMessage(file, index) {

  const formData = new FormData();
  formData.append("image", file);

  $.ajax({
    url: '/api/messages/images',
    method: 'POST',
    beforeSend: function(request) {
      request.setRequestHeader("token", localStorage.getItem('sp_token'));
    },
    data: formData,
    processData: false,
    contentType: false,
    success: (response) => {
      console.log(`Upload file ${index} successfully: `, response);
      const url = response.url;

      socket.emit(SOCKET_NAMES.SEND_MESSAGE, {
        to: currentGroup,
        message: url,
        type: 'IMAGE'
      });

      // render html image
      const msgObj = {
        url: url,
        isMime: true,
        type: 'IMAGE',
        avatar: 'http://placehold.it/50'
      };
      const html = $(htmlBuilder.generageMessage(msgObj));
      $(elements.conservation).append(html);
      scrollToBottom($(elements.conservation));
    },
    error: (error) => {
      console.log(error);
    }
  });
}

function onSelectImage(event) {
  // TODO
  const images = event.target.files;

  for(let i = 0; i < images.length; i++) {
    uploadImageAndMessage(images[i], i);
  }
}
function uploadFileAndMessage(file, index) {
  const formData = new FormData();
  formData.append('file', file);

  $.ajax({
    url: '/api/messages/files',
    method: 'POST',
    beforeSend: function (request) {
      request.setRequestHeader("token", localStorage.getItem('sp_token'));
    },
    data: formData,
    processData: false,
    contentType: false,
    success: (response) => {
      console.log(`Upload file ${index} successfully: `, response);
      const url = response.url;

      socket.emit(SOCKET_NAMES.SEND_MESSAGE, {
        to: currentGroup,
        message: url,
        type: 'FILE'
      });

      //render html file
      const msgObj = {
        url: url,
        isMime: true,
        type: 'FILE',
        avatar: 'http://placehold.it/50'
      };
      const html = $(htmlBuilder.generageMessage(msgObj));
      $(elements.conservation).append(html);
      scrollToBottom($(elements.conservation));
    },
    error: (error) => {
      console.log(error);
    }
  });
}
function onSelectFile(event) {
  const files = event.target.files;

  for (let i = 0; i < files.length; i++) {
    uploadFileAndMessage(files[i], i);
  }
}

function onChangeInputText(event) {
  const val = $(elements.inputMsg).val();
  if (val) {
    $(elements.btnSubmitMsg).removeClass('btn-secondary');
    $(elements.btnSubmitMsg).addClass('btn-primary');
  } else {
    $(elements.btnSubmitMsg).addClass('btn-secondary');
    $(elements.btnSubmitMsg).removeClass('btn-primary');
  }
}

function initEventOnChangedInputText() {
  $(elements.inputMsg).keyup((event) => {
    debounced(200, onChangeInputText)(event)
  });
}

// ===============================
// ===============================
// SOCKET

function connectToSocket() {
  socket = io(); // 1 socket phía client

  // demo emit socket
  socket.emit(SOCKET_NAMES.SET_USER_2_SOCKET, {user: JSON.parse(localStorage.getItem('sp_user'))});

  // listen event USER_ONLINE
  socket.on(SOCKET_NAMES.USER_ONLINE, function(data) {
    setOnline(data.userId);

    const user = users.find(user => user.userId === data.userId);
    if (user) {
      user.isOnline = true;
    }
  });

  socket.on(SOCKET_NAMES.USER_OFFLINE, function(data) {
    setOffline(data.userId);

    const user = users.find(user => user.userId === data.userId);
    if (user) {
      user.isOnline = false;
    }
  });

  socket.on(SOCKET_NAMES.SEND_MESSAGE, function(data) {
    console.log(SOCKET_NAMES.SEND_MESSAGE, data);
    console.log(currentGroup);

    const msgObj = {
      isMime: false,
      avatar: data.avatar,
      type: data.type
    };

    switch (data.type) {
      case 'TEXT':
        msgObj.msg = data.msg;
        break;
      case 'IMAGE':
        msgObj.url = data.msg;
        break;
      case 'FILE':
        // TODO:
        break;
    }

    const html = htmlBuilder.generageMessage(msgObj);
    $(elements.conservation).append(html);
  });
}

// ===============================
// ===============================

$(document).ready(() => {

  if (!checkExistToken()) {
    window.location.href = '/dang-nhap';
  }

  // init event onchange input message
  initEventOnChangedInputText();

  // init event mouse, keyboard
  initEventButtonSubmit();
  initEventEnterOnInputMessage();
  initEventEnterOnInputSearchUser();
  initEventClickToSelectImage();
  initEventClickToSelectFile();

  // init slim scroll
  initSlimScrollConservation();
  initSlimScrollListUser();

  // connect socket
  connectToSocket();

  // get list users and render
  setTimeout(() => {
    getListUsers();
  }, 2000);
});
