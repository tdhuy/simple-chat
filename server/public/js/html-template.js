const HTML_TEMPLATES = {
  userItem: `<div class="user-item" data-group-id="{{groupId}}" data-user-id="{{userId}}">
                <div class="user-avatar">
                    <img src="{{avatar}}" alt="">
                </div>
                <div class="user-info">
                    <div>{{name}}</div>
                    <div>{{lastMessage}} <i class="fa fa-circle"></i> <span>{{time}}</span></div>
                </div>
            </div>`,
  userItemOnline: `<div class="user-item online" data-group-id="{{groupId}}" data-user-id="{{userId}}">
                <div class="user-avatar">
                    <img src="{{avatar}}" alt="">
                </div>
                <div class="user-info">
                    <div>{{name}}</div>
                    <div>{{lastMessage}} <i class="fa fa-circle"></i> <span>{{time}}</span></div>
                </div>
            </div>`,
  theirMessage: `<div class="sp-msg their">
                    <div class="sp-msg-container">
                        <div class="sp-msg-avatar">
                            <img src="{{avatar}}" alt="">
                        </div>

                        <<message_content>>
                        
                        <div class="sm-msg-action">
                            <div class="dropdown dropup">
                                <i class="fa fa-ellipsis-h" data-toggle="dropdown" title="Khác"></i>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Xóa</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`,

  myMessage: `<div class="sp-msg mime ahihi">
                    <div class="sp-msg-container">
                        <div class="sp-msg-avatar">
                            <img src="{{avatar}}" alt="">
                        </div>

                        <<message_content>>
                        
                        <div class="sm-msg-action">
                            <div class="dropdown dropup">
                                <i class="fa fa-ellipsis-h" data-toggle="dropdown" title="Khác"></i>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Xóa</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`,

  msgContentText: `<div class="sp-msg-content">
                      {{msg}}
                  </div>`,

  msgContentFile: `<div class="sp-msg-content file">
                  <a href="{{url}}" download>
                      <i class="fa fa-file-download fa-2x"></i>
                      File
                  </a>
              </div>`,

  msgContentImage: `<div class="sp-msg-content img">
                      <img src="{{url}}" alt="">
                    </div>`,

  messageTime: `<div class="sp-msg time">
                    {{time}}
                </div>`
};
