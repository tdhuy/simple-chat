function successFn(data) {
    localStorage.setItem("sp_token", data.token);
    localStorage.setItem("sp_user", JSON.stringify(data.user));
    window.location.href = '/';
}

function errorFn(error) {
    alert(error.responseJSON.message);
}

function sendRequestAjax(body) {
    $.ajax({
        type: "POST",
        url: '/api/users/login',
        data: body,
        success: successFn,
        error: errorFn
    });
}

function getFormValues() {
    const values = $('#login-form').serializeArray();
    for (let i = 0; i< values.length; i++) {
        if (values[i].value === '') {
            alert('Vui lòng nhập đủ thông tin');
            return;
        }
    }

    const body = {};

    values.forEach((inputVal) => {
        body[inputVal.name] = inputVal.value;
    });
    console.log('body', body);
    sendRequestAjax(body);
}
function initEventOnEnter() {
    $('#email, #password').on('keypress', (event) => {
        if(event.which == 13){
            getFormValues();
        }
    });
}
function initEventClickBtnSubmit() {
    $('#btn-submit-login-form').click(() => {
        getFormValues();
    });
}

$(document).ready(() => {
    if (checkExistToken()) {
        window.location.href = '/';
    }
    initEventClickBtnSubmit();
    initEventOnEnter();
});