/**
 *
 * @typedef {Object} MessageText
 * @property {string} type
 * @property {string} msg
 * @property {string} avatar
 * @property {boolean} isMime
 *
 * @typedef {Object} MessageFile
 * @property {string} type
 * @property {boolean} isMime
 * @property {string} url
 * @property {string} fileName
 * @property {string} avatar
 *
 * @typedef {Object} MessageImage
 * @property {string} type
 * @property {boolean} isMime
 * @property {string} url
 * @property {string} avatar
 */


function _replaceVarsInTemplate(template, value) {
  const _value = JSON.parse(JSON.stringify(value));
  for (const key in _value) {
    _value[`{{${key}}}`] = value[key];
  }
  const regex = /\{\{[a-zA-Z0-9]+\}\}/gi;
  const matchedElements = template.match(regex);
  let resultTemplate = template;
  matchedElements.forEach(v => {
    resultTemplate = resultTemplate.replace(v, _value[v] || '');
  });

  return resultTemplate;
}

const htmlBuilder = {
  generateUserItem: (user) => {
    if (user.isOnline === true) {
      return _replaceVarsInTemplate(HTML_TEMPLATES.userItemOnline, user);
    }

    return _replaceVarsInTemplate(HTML_TEMPLATES.userItem, user);
  },

  /**
   *
   *
   *
   *
   * @param {MessageText | MessageFile | MessageImage} messageObject
   */
  generageMessage: (messageObject) => {
    let messageHTML = '';
    if (messageObject.isMime === true) {
      messageHTML = HTML_TEMPLATES.myMessage; // luc gui
    } else {
      messageHTML = HTML_TEMPLATES.theirMessage; // lucs nhan
    }

    switch (messageObject.type) {
      case 'TEXT':
        messageHTML = messageHTML.replace('<<message_content>>', HTML_TEMPLATES.msgContentText);
        break;
      case 'FILE':
        messageHTML = messageHTML.replace('<<message_content>>', HTML_TEMPLATES.msgContentFile);
        break;
      case 'IMAGE':
        messageHTML = messageHTML.replace('<<message_content>>', HTML_TEMPLATES.msgContentImage);
        break;
    }

    return _replaceVarsInTemplate(messageHTML, messageObject);
  }
};

/**
 * Hiển thị online với groupId hoặc userId
 * @param {string} userOrGroupId
 */
function setOnline(userOrGroupId) {
  let eles = $(elements.listUser).find(`[data-group-id=${userOrGroupId}]`);
  if (eles.length === 0) {
    eles = $(elements.listUser).find(`[data-user-id=${userOrGroupId}]`);
  }

  eles.addClass('online');
}

/**
 * Hiển thị offline với groupId hoặc userId
 * @param {string} userOrGroupId
 */
function setOffline(userOrGroupId) {
  let eles = $(elements.listUser).find(`[data-group-id=${userOrGroupId}]`);
  if (eles.length === 0) {
    eles = $(elements.listUser).find(`[data-user-id=${userOrGroupId}]`);
  }

  eles.removeClass('online');
}

/**
 * Init event on click button submit message
 */
function initEventButtonSubmit() {
  $(elements.btnSubmitMsg).click(() => {
    sendMessage();
  });
}

/**
 * Init event enter on input message
 */
function initEventEnterOnInputMessage() {
  $(elements.inputMsg).keyup((event) => {
    if (event.which === KEY_CODES.ENTER) {
      sendMessage();
    }
  });
}

/**
 * Init event enter on input search user
 */
function initEventEnterOnInputSearchUser() {
  $(elements.inputSearchUser).keyup((event) => {
    if (event.which === KEY_CODES.ENTER) {
      filterUsers();
    }
  });
}

/**
 *
 * @param {boolean} isLoading
 */
function toggleLoaderSendingMsg(isLoading) {
  if (isLoading) {
    $(elements.btnSubmitMsg).addClass('loading');
  } else {
    $(elements.btnSubmitMsg).removeClass('loading');
  }
}

/**
 *
 * @param {boolean} isLoading
 */
function toggleLoaderLoadingUserList(isLoading) {
  if (isLoading) {
    $(elements.listUser).addClass('loading');
  } else {
    $(elements.listUser).removeClass('loading');
  }
}


/**
 *
 * @param {boolean} isLoading
 */
function toggleLoaderConservation(isLoading) {
  if (isLoading) {
    $(elements.mainContent).addClass('loading');
  } else {
    $(elements.mainContent).removeClass('loading');
  }
}

function initSlimScrollListUser() {
  $(elements.listUser).slimScroll({
    height: $(elements.listUser).height() + 'px',
    width: $(elements.listUser).width() + 'px',
    alwaysVisible: false
  });
}

function initSlimScrollConservation() {
  $(elements.conservation).slimScroll({
    height: $(elements.conservation).height() + 'px',
    width: $(elements.conservation).width() + 'px',
    alwaysVisible: false,
    start: 'bottom'
  });
}

function scrollToBottom(ele) {
  ele.slimScroll({
    scrollTo: ele[0].scrollHeight + 'px'
  });
}

function initEventClickToSelectImage() {
  $('#input-image').change((event) => {
    onSelectImage(event);
  });

  $('#btn-input-image').click(() => {
    $('#input-image').trigger('click');
  });
}

function initEventClickToSelectFile() {
  $('#input-file').change((event) => {
    onSelectFile(event);
  });

  $('#btn-input-file').click(() => {
    $('#input-file').trigger('click');
  });
}

function checkExistToken() {
  const token = localStorage.getItem("sp_token");
  const userStr = localStorage.getItem("sp_user");

  if (!token) {
    return false;
  }

  if (!userStr) {
    return false;
  }

  try {
    const user = JSON.parse(userStr);
  } catch (e) {
    return false;
  }

  return true;
}

function debounced(delay, fn) {
  let timerId;
  return function (...args) {
    if (timerId) {
      clearTimeout(timerId);
    }
    timerId = setTimeout(() => {
      fn(...args);
      timerId = null;
    }, delay);
  }
}
