function successFn(data) {
    alert(data.message);
    window.location.href = '/';
}
function errorFn(error) {
    alert(error.responseJSON.message);
}
function sendRequestAjax(body) {
    $.ajax({
        type: "POST",
        url: '/api/users/register',
        data: body,
        success: successFn,
        error: errorFn
    });
}
function getFormValues() {
    const values = $('#register-form').serializeArray();
    for(let i = 0; i< values.length ; i++){
        if(values[i].value === ''){
            alert('Vui lòng nhập đủ thông tin');
            return;
        }
    }
    const body = {};
    values.forEach((inputVal) =>{
        body[inputVal.name] = inputVal.value;
    });
    sendRequestAjax(body);
}
function initEventClickBtnSubmit() {
    $('#btn-submit-register-form').click(() =>{
        getFormValues();
    });
}
$(document).ready(() => {
    if(checkExistToken()){
        window.location.href = '/';
    }
    initEventClickBtnSubmit()
});