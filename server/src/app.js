const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const HttpStatus = require('http-status-codes');

const app = express();
const corsOptionsDelegate = function (req, callback) {
	let corsOptions;
	corsOptions = { origin: true, credentials: true }; // reflect (enable) the requested origin in the CORS response
	callback(null, corsOptions) // callback expects two parameters: error and options
};
app.use(cors(corsOptionsDelegate));
app.use(express.static('public'));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api', require('./routes/api'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	return res
		.status(HttpStatus.NOT_IMPLEMENTED)
		.json({
		message: 'Unsupported api'
	})
});

// error handler
app.use(function (err, req, res, next) {
	const msg = err.message ? err.message : JSON.stringify(err);

	return res
		.status(HttpStatus.INTERNAL_SERVER_ERROR)
		.json({
			messages: msg
		});
});

module.exports = app;
