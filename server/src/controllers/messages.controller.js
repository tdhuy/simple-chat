const mongoose = require('mongoose');
const messagesModel = require('../models/messages.model');
const fs = require('fs');

const loadMessages = async (req, res) => {
    try {
        const groupId = req.query.groupId;
        const results = [];
        if (!groupId) {
            return res.status(400).json({
                message: "Not enough data"
            });
        }

        const messages = await messagesModel.find({groupId: groupId})
            .sort({createdAt: 1})
            .lean();
        if (!messages) {
            return res.status(400).json({
                message: "Thất bại",
            });
        }

        messages.forEach(msg => {
            msg.isMime = msg.userId.toString() === req.user._id.toString();
            results.push({...msg});
        });

        return res.status(200).json({
            message: "Thành công",
            data: results
        });
    } catch (e) {
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};
const deleteMessages = async (req, res) => {
    try {
        const message = await messagesModel.findOne({_id: req.params.id});
        if (!message) {
            return res.status(400).json({
                message: 'Không có message'
            });
        }

        if (message.userId.toString() !== req.user._id.toString()) {
            return res.status(400).json({
                message: 'Không có quyền xóa message'
            });
        }

        await message.remove();

        return res.status(200).json({
            message: 'Xóa message thành công'
        });
    } catch (e) {
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};
const uploadImg = async (req, res) => {
    try {
        const image = req.file;
        if (image === null || image === undefined) {
            return res.status(400).json({
                message: 'Không có hình'
            });
        }

        if (image.mimetype !== 'image/jpeg' && image.mimetype !== 'image/png') {
            removeImg(req);
            return res.status(400).json({
                message: 'Hình ảnh không đúng định dạng'
            });
        }

        const oldPath = '/../public/tmp/' + req.file.filename;
        const newPath = '/../public/uploads/' + req.file.filename;
        fs.renameSync(__dirname + oldPath, __dirname + newPath);

        return res.status(200).json({
            message: 'Gửi hình thành công',
            url: `uploads/${req.file.filename}`
        });
    } catch (e) {
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};

const removeImg = async (req, res) => {
    try {
        const imagePath = '/../public/tmp/' + req.file.filename;
        fs.unlinkSync(__dirname + imagePath);
    } catch (e) {
        console.log(e);
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};

const uploadFiles = async (req, res) => {
    try {
        const file = req.file;
        if (file == null || file == undefined) {
            return res.status(400).json({
                message: 'Không có file'
            });
        }
        if (file.mimetype !== 'application/vnd.ms-excel' && file.mimetype !== 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
            removeImg(req);
            return res.status(400).json({
                message: 'File không đúng định dạng'
            });
        }
        const oldPath = '/../public/tmp/' + req.file.filename;
        const newPath = '/../public/uploads/' + req.file.filename;
        fs.renameSync(__dirname + oldPath, __dirname + newPath);

        return res.status(200).json({
            message: 'Gửi file thành công',
            url: `uploads/${req.file.filename}`
        });
    } catch (e) {
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};

module.exports = {
    loadMessages,
    deleteMessages,
    uploadImg,
    uploadFiles
};
