const mongoose = require('mongoose');
const usersModel = require('../models/users.model');
const groupsModel = require('../models/groups.model');
const fs = require('fs');

const createGroups = async (req, res) => {
    try{
        const user = await usersModel.findOne({_id: req.body.userId});
        if(!user){
            return res.status(400).json({
                message: 'User không tồn tại'
            });
        }
        const userIds = [req.user._id, req.body.userId].map(userId => {
            return new mongoose.Types.ObjectId(userId);
        });
        const duplicateGroup = await groupsModel.findOne({
            userIds: {
                $all: userIds,
                $size: 2
            }
        });
        if(duplicateGroup){
            return res.status(200).json({
                message: 'Thành công',
                data: {
                    group: duplicatedGroup
                }
            });
        }
        const group = new groupsModel({
            userIds: userIds
        });
        await group.save();
        return res.status(200).json({
            message: 'Thành công',
            data: {
                group: group
            }
        });
    }catch (e) {
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};


// handle as a middleware
const checkValidGroup = async (req, res, next) => {
    try {
        const groupId = req.params.groupId;
        const group = await groupsModel.findOne({_id: groupId});

        if (!group) {
            return res.status(404).json({
                message: 'Thao tác không hợp lệ. Không tìm thấy nhóm'
            });
        }

        req.group = group;
        return next();
    } catch (e) {
        console.log(e);
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};

// handle as a middleware
const checkBelongToGroup = async (req, res, next) => {
    try {
        if (req.group.userIds.indexOf(req.user._id) === -1) {
            return res.status(500).json({
                message: 'Thao tác không hợp lệ. Bạn không thuộc group này'
            });
        }

        return next();
    } catch (e) {
        console.log(e);
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};

const updateGroupAvatar = async (req, res, next) => {
    try {
        const oldImagePath = __dirname + `/../public/${req.group.avatar}`;
        if (fs.existsSync(oldImagePath)) {
            fs.unlinkSync(oldImagePath);
        }

        req.group.avatar = `images/uploads/groups/${req.file.filename}`;
        await req.group.save();

        return res.status(200).json({
            message: 'Cập nhật avatar thành công'
        });
    } catch (e) {
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};

const updateInfo = async (req, res, next) => {
    try {
        const {name} = req.body;
        req.group.name = name;
        await req.group.save();

        return res.json({
            message: 'Cập nhật tên nhóm thành công'
        });
    } catch (e) {
        return next(e);
    }
};

module.exports = {
    createGroups,
    checkValidGroup,
    checkBelongToGroup,
    updateGroupAvatar,
    updateInfo
};
