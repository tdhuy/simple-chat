const bcrypt = require('bcrypt');
const groupModel = require('../models/groups.model');
const usersModel = require('../models/users.model');
const tokenModel = require('../models/tokens.model');
const randomString = require('randomstring');
const fs = require('fs');
const HttpStatusCodes = require('http-status-codes');
const messagesModel = require('../models/messages.model');

const checkValidatorEmail = (emailAdress) => {
    var filter = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
    return filter.test(emailAdress);
};

const registerUser = async (req, res) => {
    try {
        const { email, password, confirmedPassword, name } = req.body;
        if (!email || !password || !confirmedPassword || !name) {
            return res.status(400).json({
                message: 'Vui lòng điền đầy đủ thông tin'
            });
        }

        if (password !== confirmedPassword) {
            return res.status(400).json({
                message: 'Hai mật khẩu không giống nhau'
            });
        }

        if (!checkValidatorEmail(email)) {
            return res.status(400).json({
                message: 'Email không đúng định dạng'
            });
        }

        const user = await usersModel.findOne({ email: email });
        if (user !== null && user !== undefined) {
            return res.status(400).json({
                message: 'Trùng email'
            });
        }

        const saltRounds = bcrypt.genSaltSync(10);
        const hashedPassword = bcrypt.hashSync(password, saltRounds);
        const userdb = new usersModel({
            email: email,
            passwordSalt: saltRounds,
            hashedPassword: hashedPassword,
            name: name,
            avatar: "",
            socketId: ""
        });
        await userdb.save();

        return res.status(200).json({
            message: 'Đăng ký thành công'
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            message: JSON.stringify(err)
        });
    }
};

const loginUser = async (req, res) => {
    try {
        const { email, password } = req.body;
        // kiểm tra input rỗng
        if (!email || !password) {
            return res.status(400).json({
                message: 'Email hoặc Password không đúng'
            });
        }

        //kiểm tra user đã tồn tại chưa
        const user = await usersModel.findOne({ email: email });
        if (user === null || user === undefined) {
            return res.status(400).json({
                message: 'Tài khoản không tồn tại'
            });
        }

        //kiểm tra password
        if (bcrypt.compareSync(password, user.hashedPassword) === false) {
            return res.status(400).json({
                message: 'Password không đúng'
            });
        }

        const tokenString = randomString.generate(20);
        const tokenUser = new tokenModel({
            userId: user._id,
            token: tokenString,
            expiredAt: Date.now()
        });
        await tokenUser.save();

        return res.status(200).json({
            message: 'Đăng nhập thành công',
            token: tokenUser.token,
            user: {
                email: user.email,
                name: user.name,
                avatar: user.avatar || ''
            }
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            message: JSON.stringify(err)
        });
    }
};

const getListUsers = async (req, res, next) => {
    try {
        const results = [];
        const usersGroups = await usersModel.aggregate([
            {
                $match: {
                    _id: {
                        $ne: req.user._id
                    }
                },
            },
            {
                $project: {
                    _id: 1,
                    name: 1,
                    email: 1,
                    isOnline: 1
                }
            },
            {
                $lookup: {
                    "from": "groups",
                    "localField": "_id",
                    "foreignField": "userIds",
                    "as": "groups"
                }
            },
            {
                $unwind: {
                    path: "$groups"
                }
            },
            {
                $match: {
                    "groups.userIds": {
                        $in: [req.user._id]
                    }
                }
            }
        ]);
        const exceptUserIds = usersGroups.map(g => g._id);
        const users = await usersModel
            .find({
                _id: {
                    $nin: [...exceptUserIds, req.user._id]
                }
            })
            .select('_id email name avatar isOnline')
            .lean();
        users.forEach(u => {
            results.push({
                ...u,
                type: 'USER',
                isOnline: u.isOnline
            });
        });
        usersGroups.forEach(g => {
            results.push({
                ...g.groups,
                name: g.name,
                type: 'GROUP',
                isOnline: g.isOnline
            })
        });

        await Promise.all(usersGroups.map(async (g) => {
            const lastMessage = await messagesModel.find({ groupId: g.groups._id })
                .sort({ createdAt: -1 })
                .limit(1);
            return lastMessage;
        }));

        return res.status(200).json({
            message: 'Thành công',
            data: {
                groupsAndUsers: results
            }
        });
    } catch (e) {
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};

const uploadAvatar = async (req, res, next) => {
    try {
        const oldImagePath = __dirname + `/../public/${req.user.avatar}`;
        if (req.user.avatar && fs.existsSync(oldImagePath)) {
            fs.unlinkSync(oldImagePath);
        }

        req.user.avatar = `images/uploads/users/${req.file.filename}`;
        await req.user.save();

        return res.status(200).json({
            message: 'Cập nhật avatar thành công',
            data: {
                avatar: req.user.avatar
            }
        });
    } catch (e) {
        console.error(e);
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};

const updateInfo = async (req, res, next) => {
    try {
        const { name } = req.body;
        let haveChanged = false;

        if (name) {
            req.user.name = name;
            haveChanged = true;
        }

        if (haveChanged) {
            await req.user.save();
        }

        return res.status(200).json({
            message: 'Thành công'
        })
    } catch (e) {
        console.error(e);
        return res.status(500).json({
            message: JSON.stringify(e)
        });
    }
};

const updatePassword = async (req, res, next) => {
    try {
        const { oldPassword, password } = req.body;

        if (bcrypt.compareSync(oldPassword, req.user.hashedPassword) === false) {
            return res
                .status(HttpStatusCodes.BAD_REQUEST)
                .json({
                    message: 'Mật khẩu cũ không đúng'
                });
        }

        req.user.hashedPassword = bcrypt.hashSync(password, req.user.passwordSalt);
        await req.user.save();

        return res
            .status(HttpStatusCodes.OK)
            .json({
                message: 'Thay đổi mật khẩu thành công'
            })
    } catch (e) {
        return next(e);
    }
};

module.exports = {
    registerUser,
    loginUser,
    getListUsers,
    uploadAvatar,
    updateInfo,
    updatePassword
};
