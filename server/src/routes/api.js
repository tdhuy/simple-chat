const express = require('express');
const router = express.Router({});
const usersRouter = require('./users.route');
const groupsRouter = require('./groups.route');
const messagesRouter = require('./messages.route');
router.use('/users', usersRouter);
router.use('/groups', groupsRouter);
router.use('/messages', messagesRouter);
module.exports = router;
