const express = require('express');
const router = express.Router({});
const controllers = require('../controllers/groups.controller');
const checkToken = require('../middlewares/check-token.middleware');
const multer = require('multer');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, 'public/images/uploads/groups');
	},
	filename: (req, file, cb) => {
		cb(null, req.group._id + '-' + Date.now() + '-' + file.originalname);
	}
});
const upload = multer({ storage: storage });

// tạo group cho user
router.post('/', checkToken, controllers.createGroups);

router.post('/:groupId/avatar',
	checkToken,
	controllers.checkValidGroup,
	controllers.checkBelongToGroup,
	upload.single('image'),
	controllers.updateGroupAvatar
);

router.put('/:groupId/info', [
	checkToken,
	controllers.checkValidGroup,
	controllers.checkBelongToGroup,
	controllers.updateInfo
]);

module.exports = router;
