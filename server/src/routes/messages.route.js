const express = require('express');
const router = express.Router({});
const controllers = require('../controllers/messages.controller');
const checkToken = require('../middlewares/check-token.middleware');
const multer = require('multer');
const fs = require('fs');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        const tmp = './public/tmp';
        if (!fs.existsSync(tmp)){
            fs.mkdirSync(tmp);
        }

        const uploads = './public/uploads';
        if (!fs.existsSync(uploads)){
            fs.mkdirSync(uploads);
        }

        cb(null, __dirname + '/../public/tmp');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() +'_'+ file.originalname);
    }
});
const upload = multer({
    storage: storage
});

router.post('/images', checkToken, upload.single('image'), controllers.uploadImg);
router.post('/files', checkToken, upload.single('file'), controllers.uploadFiles);
router.get('/', checkToken, controllers.loadMessages);
router.delete('/:id', checkToken, controllers.deleteMessages);

module.exports = router;
