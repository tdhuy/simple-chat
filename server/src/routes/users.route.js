const express = require('express');
const router = express.Router({});
const controllers = require('../controllers/users.controller');
const checkToken = require('../middlewares/check-token.middleware');
const multer = require('multer');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, 'public/images/uploads/users');
	},
	filename: (req, file, cb) => {
		cb(null, req.user._id.toString() + '-' + Date.now() + '-' + file.originalname);
	}
});
const upload = multer({
	storage: storage,
	limits: { fieldSize: 25 * 1024 * 1024 }
});

router.post('/register', controllers.registerUser);
router.post('/login', controllers.loginUser);
router.post('/avatar', checkToken, upload.single('image'), controllers.uploadAvatar);
router.put('/', checkToken, controllers.updateInfo);

// lấy danh sách user
router.get('/', checkToken, controllers.getListUsers);

module.exports = router;
