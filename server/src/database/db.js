const mongoose = require('mongoose');

module.exports = (callback) => {
  mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true }, function (err) {
    if (err) {
      throw err;
    } else {
      callback();
    }
  });
};
