const tokenModel = require('../models/tokens.model');
const userModel = require('../models/users.model');

module.exports = async (req, res, next) => {
  const token = req.headers['token'] || req.query.token || req.body.token;

  if (!token) {
      return res.status(401).json({
          message: 'Cần đăng nhập'
      });
  }

  const userToken = await tokenModel.findOne({token});
  if (!userToken) {
      return res.status(401).json({
          message: 'Token không hợp lệ'
      });
  }

  const user = await userModel.findOne({_id: userToken.userId});
  if (!user) {
      return res.status(401).json({
          message: 'Token không hợp lệ'
      });
  }

  req.user = user;
  return next();
};