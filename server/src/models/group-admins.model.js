const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const groupAdminSchema = new Schema({
    userId: ObjectId,
    groupId: ObjectId
}, {timestamps: true});
const GroupAdmin = new mongoose.model('GroupAdmin', groupAdminSchema);
module.exports = GroupAdmin;