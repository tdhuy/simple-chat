const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userSchema = new Schema({
		email         : String,
		passwordSalt  : String,
		hashedPassword: String,
		name          : String,
		avatar        : String,
		isOnline      : {
			type   : Boolean,
			default: false
		},
		timeOnline    : Date,
		socketId      : String
	},
	{ timestamps: true }
);

const User = new mongoose.model('User', userSchema);
module.exports = User;

