const mongoose = require ('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const messageSchema = new Schema({
    groupId: ObjectId,
    userId: ObjectId,
    text: String,
    image: String,
    file: String,
    type: String
}, {timestamps: true});
const Message = new mongoose.model('Message', messageSchema);
module.exports = Message;