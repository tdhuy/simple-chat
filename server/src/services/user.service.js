const userModel = require('../models/users.model');

const updateSocketIdAndStatusOnline = async (email, socketId) => {
	const user = await userModel.findOne({email});

	if (user) {
		user.socketId = socketId;
		user.timeOnline = new Date();
		user.isOnline = true;
		await user.save();

		return user;
	}

	return null;
};

const findUserBySocketId = async (socketId) => {
	return userModel.findOne({socketId});
};

module.exports = {
	updateSocketIdAndStatusOnline,
	findUserBySocketId
};
