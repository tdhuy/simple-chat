const mongoose = require('mongoose');
const MessageModel = require('../models/messages.model');

const createMessageForGroup = async (messageInfo) => {

	const {groupId, userId, text, image, file, type} = messageInfo;

	const msg = new MessageModel({
		groupId: new mongoose.Types.ObjectId(groupId),
		userId: new mongoose.Types.ObjectId(userId),
		text: text,
		image: image,
		file: file,
		type
	});

	return await msg.save();
};

module.exports = {
	createMessageForGroup: createMessageForGroup
};
