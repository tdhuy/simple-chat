const groupModel = require('../models/groups.model');
const messageModel = require('../models/messages.model');
const userModel = require('../models/users.model');
const UserService = require('./user.service');
const MessageService = require('./message.service');
const SOCKET_NAMES = require('../../socket-names.json');

let globalIO = null;

async function joinGroupsOfUser(user, socket) {
	if (user) {
		const groups = await groupModel.find({ userIds: user._id });
		groups.forEach(group => {
			socket.join(group._id.toString());
		});
	}
}

async function emitUserOnline(email) {
	const user = await userModel.findOne({email});
	if (user) {
		globalIO.emit(SOCKET_NAMES.USER_ONLINE, {userId: user._id.toString()});
	}
}

function setUserWhenConnectSocket(socket) {
	return async (data) => {
		try {
			if (data.user.email) {
				const user = await UserService.updateSocketIdAndStatusOnline(data.user.email, socket.id);
				await emitUserOnline(data.user.email);

				await joinGroupsOfUser(user, socket);
			}

		} catch (e) {
			console.error(e);
		}
	}
}

function setUserWhenDisconnect(socket) {
	return async () => {
		const user = await UserService.findUserBySocketId(socket.id);
		if (user) {
			user.isOnline = false;
			user.socketId = '';
			await user.save();
			await emitUserOffline(user.email);
		}
	}
}

async function emitUserOffline(email) {
	const user = await userModel.findOne({email});
	if (user) {
		globalIO.emit(SOCKET_NAMES.USER_OFFLINE, {userId: user._id.toString()});
	}
}

function onReceiveMessage(socket) {
	return async (data) => {
		console.log('====> SEND MESSAGE');
		console.log('Socket sender: ', socket.id);
		console.log('Content: ', JSON.stringify(data.message));

		if (!data.to) {
			console.error('Not enough data: ', data);
			return;
		}

		const user = await UserService.findUserBySocketId(socket.id);
		if (!user) {
			return;
		}

		const msg = await MessageService.createMessageForGroup(
			{
				groupId: data.to,
				userId: user._id.toString(),
				text: data.type === 'TEXT' ? data.message : undefined,
				image: data.type === 'IMAGE' ? data.message : undefined,
				file: data.type === 'FILE' ? data.message : undefined,
				type: data.type
			}
		);

		// TODO: send message to group except sender
		socket.broadcast.to(data.to).emit(SOCKET_NAMES.SEND_MESSAGE, {
			avatar: user.avatar,
			msg: data.message,
			groupId: data.to,
			type: data.type
		});
	};
}


function setIOInstance(io) {
	globalIO = io;
}

module.exports = {
	joinGroupsOfUser,
	setIOInstance,
	setUserWhenConnectSocket,
	setUserWhenDisconnect,
	emitUserOffline,
	onReceiveMessage
};
