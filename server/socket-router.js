const SOCKET_NAMES = require('./socket-names.json');
const SocketService = require('./src/services/socket.service');

module.exports = (io) => {
	SocketService.setIOInstance(io);

	io.on('connection', function (socket) {
		socket.on(SOCKET_NAMES.SET_USER_2_SOCKET, SocketService.setUserWhenConnectSocket(socket));

		socket.on('disconnect', SocketService.setUserWhenDisconnect(socket));

		socket.on(SOCKET_NAMES.SEND_MESSAGE, SocketService.onReceiveMessage(socket));
	});
};
